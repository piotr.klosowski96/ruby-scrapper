require 'optparse'
require 'net/http'
require 'nokogiri'
require 'json'
require 'i18n'

results = []
options = {}
OptionParser.new do |opt|
  opt.on('--minimum_price MINIMUM_PRICE') { |o| options["search%5Bfilter_float_price%3Afrom%5D"] = o }
  opt.on('--maximum_price MAXIMUM_PRICE') { |o| options["search%5Bfilter_float_price%3Ato%5D"] = o }
  opt.on('--minimum_area MINIMUM_AREA') { |o| options["search%5Bfilter_float_m%3Afrom%5D"] = o }
  opt.on('--maximum_area MAXIMUM_AREA') { |o| options["search%5Bfilter_float_m%3Ato%5D"] = o }
  opt.on('--output_file OUTPUT_FILE') { |o| options["output_file"] = o }
end.parse!

base_url="https://www.otodom.pl/sprzedaz/mieszkanie/krakow/"

unless options.empty?
  base_url += "?"
  options.each do |key, value|
    base_url += "&" + key + "=" + value
  end
end

uri = URI.parse(base_url)
response = Net::HTTP.get_response(uri).body
response = Nokogiri::HTML(response)

pages = response.at_css("[id=\"pagerForm\"]").at_css("[class=\"pager\"]").element_children
pages_count = pages.map { |p| p.content.to_i }.max

def process_input(input_text)
  I18n.available_locales = [:pl]
  I18n.locale = :pl
  I18n.transliterate(
    input_text.
    downcase.
    gsub(/(.*)( m²)/, '\1'). # remove ' m²'
    gsub(/(.*)( zł)/, '\1'). # remove ' zł'
    gsub(/(\d+)(,)(\d*)/, '\1.\3') # use dot as floating point separator
    )
end

(1..pages_count).each do |page|
  uri = URI.parse(base_url + "&page=" + page.to_s)
  response = Nokogiri::HTML(Net::HTTP.get_response(uri).body)

  response.xpath('//article[starts-with(@id, "offer-item-ad_id")]').each do |offer|
    offer_uri = URI.parse(offer['data-url'])
    offer_body = Nokogiri::HTML(Net::HTTP.get_response(offer_uri).body)
    offer_details = {}

    offer_details["url"] = offer['data-url']
    price = process_input(offer_body.css("//strong[aria-label=\"Cena\"]").text)
    offer_details["price"] = price
    offer_body.css("//div[role=\"region\"]//div").
      map { |node| node.content }.
      each_slice(2).
      to_a.
      each do |key, value|
        offer_details[process_input(key).tr(" ", "_")] = process_input(value)
    end

    results.append(offer_details)
  end
end

File.write(options["output_file"] + ".json", JSON.generate(results), mode: "w")
